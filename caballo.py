import numpy as np
tablero = []
numFilas=8
numColumnas=8


num = 1
tablero = [0] * numFilas
for i in range(numFilas):
    tablero[i] = [0] * numColumnas

fila = int(input("Digite la fila: "))
columna = int(input("Digite la columna: "))

tablero[fila][columna] = num

def movimiento(fila, columna):
    temporal = []
    posiciones = []
    if fila + 2 <= 7 and columna + 1 <= 7 and tablero[fila + 2][columna + 1] == 0:
        temporal.append(fila + 2)
        temporal.append(columna + 1)
        posiciones.append(temporal)
        temporal = []
    if fila + 1 <= 7 and columna + 2 <= 7 and tablero[fila + 1][columna + 2] == 0:
        temporal.append(fila + 1)
        temporal.append(columna + 2)
        posiciones.append(temporal)
        temporal = []
    if fila + 2 <= 7 and columna - 1 >= 0 and tablero[fila + 2][columna - 1] == 0:
        temporal.append(fila + 2)
        temporal.append(columna - 1)
        posiciones.append(temporal)
        temporal = []
    if fila + 1 <= 7 and columna - 2 >= 0 and tablero[fila + 1][columna - 2] == 0:
        temporal.append(fila + 1)
        temporal.append(columna - 2)
        posiciones.append(temporal)
        temporal = []
    if fila - 2 >= 0 and columna + 1 <= 7 and tablero[fila - 2][columna + 1] == 0:
        temporal.append(fila - 2)
        temporal.append(columna + 1)
        posiciones.append(temporal)
        temporal = []
    if fila - 1 >= 0 and columna + 2 <= 7 and tablero[fila - 1][columna + 2] == 0:
        temporal.append(fila - 1)
        temporal.append(columna + 2)
        posiciones.append(temporal)
        temporal = []
    if fila - 2 >= 0 and columna - 1 >= 0 and tablero[fila - 2][columna - 1] == 0:
        temporal.append(fila - 2)
        temporal.append(columna - 1)
        posiciones.append(temporal)
        temporal = []
    if fila - 1 >= 0 and columna - 2 >= 0 and tablero[fila - 1][columna - 2] == 0:
        temporal.append(fila - 1)
        temporal.append(columna - 2)
        posiciones.append(temporal)

    return posiciones


for i in range(2,(numFilas*numColumnas)+1):
    var = movimiento(fila, columna)
    variable = []
    variable2 = []
    menor = 0
    for j in range(len(var)):
        variable.append(len(movimiento(var[j][0], var[j][1])))
        variable2.append(len(movimiento(var[j][0], var[j][1])))
    variable2.sort()
    variable=np.array(variable)
    menor = np.argmin(variable)
    tablero[var[menor][0]][var[menor][1]] = i
    fila=var[menor][0]
    columna=var[menor][1]


for i in tablero:
  print(i)

