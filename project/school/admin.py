from django.contrib import admin
from .models import Student, Teacher, Course, Subject, Registration, Score 


class AuthorAdmin(admin.ModelAdmin):
    pass


admin.site.register(Teacher, AuthorAdmin)


@admin.register(Student)
class Student(admin.ModelAdmin):
    pass

@admin.register(Subject)
class Subject(admin.ModelAdmin):
    pass


@admin.register(Course)
class Course(admin.ModelAdmin):
    pass


@admin.register(Score)
class Score(admin.ModelAdmin):
    pass


@admin.register(Registration)
class Registration(admin.ModelAdmin):
    pass


#admin.site.register(Student)




