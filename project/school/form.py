from django import forms
from .models import Student, Teacher, Subject, Course, Registration
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class StudentForm(forms.ModelForm):
    class Meta:
        model = Student

        fields = [
            'name',
            'last_name',
            'age',
            'email',
        ]
        labels = {
            'name': 'Name',
            'last_name': 'Last Name',
            'age': 'Age',
            'email': 'Email',
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': 'validate'}),
            'last_name': forms.TextInput(attrs={'class': 'validate'}),
            'age': forms.NumberInput(attrs={'class': 'validate'}),
            'email': forms.TextInput(attrs={'class': 'validate'}),
        }


class TeacherForm(forms.ModelForm):
    class Meta:
        model = Teacher

        fields = [
            'name',
            'last_name',
            'email',
        ]
        labels = {
            'name': 'Name',
            'last_name': 'Last Name',
            'email': 'Email',
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': 'validate'}),
            'last_name': forms.TextInput(attrs={'class': 'validate'}),
            'email': forms.TextInput(attrs={'class': 'validate'}),
        }


class SubjectForm(forms.ModelForm):
    class Meta:
        model = Subject

        fields = [
            'name',
            'id_teacher',
        ]
        labels = {
            'name': 'Name',
            'id_teacher': 'Select Teacher'
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': 'validate'}),
            'id_teacher': forms.Select(),
        }


class CourseForm(forms.ModelForm):
    class Meta:
        model = Course

        fields = [
            'grade',

        ]
        labels = {
            'grade': 'Grade Name',
        }
        widgets = {
            'grade': forms.TextInput(attrs={'class': 'validate'}),
        }


class RegistrationForm(forms.ModelForm):
    class Meta:
        model = Registration

        fields = [
            'student_id',
            'subject_id',
            'course_id',

        ]
        labels = {
            'student_id': 'Student ID',
            'subject_id': 'Subject ID',
            'course_id': 'Course ID',
        }
        widgets = {
            'student_id': forms.Select(),
            'subject_id': forms.Select(),
            'course_id': forms.Select(),
        }


class UserForm(UserCreationForm):
    class Meta:
        model = User

        fields = [

            'password1',
            'password2',

        ]
        label = {

            'password1': 'Password',
            'password2': 'Confirm Password',
        }
