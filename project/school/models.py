from django.db import models
from django.contrib.auth.models import User


class Student(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    age = models.IntegerField()
    email = models.EmailField(max_length=50)

    def __str__(self):
        return self.name


class Teacher(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(max_length=50)

    def __str__(self):
        return '{} {}'.format(self.name, self.last_name)


class Course(models.Model):
    id = models.AutoField(primary_key=True)
    grade = models.CharField(max_length=30)

    def __str__(self):
        return '{}'.format(self.grade)


class Subject(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30)
    id_teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.name)


class Registration(models.Model):
    id = models.AutoField(primary_key=True)
    student_id = models.ForeignKey(Student, on_delete=models.CASCADE)
    subject_id = models.ForeignKey(Subject, on_delete=models.CASCADE)
    course_id = models.ForeignKey(Course, on_delete=models.CASCADE)


class Score(models.Model):
    id = models.AutoField(primary_key=True)
    registration_id = models.ForeignKey(Registration, on_delete=models.CASCADE)
    score1 = models.FloatField()
    score2 = models.FloatField()
    score3 = models.FloatField()
    final_score = models.FloatField()

