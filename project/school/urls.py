from django.contrib.auth.decorators import login_required
from . import views
from .views import StudentsViews, TeachersViews, AdminViews, AddStudentCreateView, ConfigStudentEditView, \
    AddTeacherCreateView
from django.conf.urls import url,include
from .views import Login
from django.conf import settings
from django.contrib.auth.views import logout
from rest_framework import routers
from.views import UserViewSet,GroupViewSet

router=routers.DefaultRouter()
router.register(r'users',UserViewSet)
router.register(r'groups',GroupViewSet)
app_name = 'school'

urlpatterns = [
    url('',include(router.urls)),
    url(r'^$', Login.as_view(), name='school'),
    url(r'^logout/$', logout, {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    url(r'^student/$', StudentsViews.student_view, name='student'),
    url(r'^teacher/$', TeachersViews.teacher_view, name='teacher'),
    url(r'^teacher/subject/(?P<pk>\d+)$', TeachersViews.teacher_subjects_view, name='subject'),
    url(r'^teacher/subject/student$', TeachersViews.teacher_subject_student_view, name=''),
    url(r'^config/$', AdminViews.configView, name='config'),
    url(r'^config/students$', AdminViews.configStudentsView, name='configStudents'),
    url(r'^config/student/add$', AddStudentCreateView.as_view(), name='configStudentAdd'),
    url(r'^config/student/edit/(?P<pk>\d+)$', ConfigStudentEditView.as_view(), name='configStudentEdit'),
    url(r'^config/teachers$', AdminViews.configTeachersView, name='configTeachers'),
    url(r'^config/teacher/add$', AddTeacherCreateView.as_view(), name='configTeacherAdd'),
    url(r'^config/teacher/edit$', AdminViews.configTeacherEditView, name='configTeacherEdit'),
    url(r'^config/subjects$', AdminViews.configSubjectsView, name='configSubjects'),
    url(r'^config/subject/add$', AdminViews.configSubjectAddView, name='configSubjectsAdd'),
    url(r'^config/subject/edit$', AdminViews.configSubjectEditView, name='configSubjectsEdit'),
    url(r'^config/cursos$', AdminViews.configCursosView, name='configCursos'),
    url(r'^config/curso/add$', AdminViews.configCursosAddView, name='configCursosAdd'),
    url(r'^config/curso/edit$', AdminViews.configCursosEditView, name='configCursosEdit'),
    url(r'^config/enrollments$', AdminViews.configEnrollmentsView, name='configEnrollments'),
    url(r'logged/$', StudentsViews.student_view, name='logged'),
    url(r'^config/enrollment/add$', AdminViews.configEnrollmentsAddView, name='configEnrollmentsAdd'),
    url(r'^config/enrollment/edit$', AdminViews.configEnrollmentsEditView, name='configEnrollmentsEdit'),


]

