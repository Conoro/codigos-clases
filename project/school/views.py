from .models import Student, Teacher, Student, Score, Course, Registration
from django.contrib.auth import login as auth_login, authenticate
from django.contrib.auth.models import User,Group
from django.views import View
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from .models import Student, Teacher, Subject, Course, Registration
from .form import StudentForm, UserForm, TeacherForm, SubjectForm, CourseForm, RegistrationForm
from django.http import HttpResponseRedirect
from django.views.generic import CreateView, UpdateView
from django.urls import reverse_lazy
from django.contrib.auth.models import Group, User
from rest_framework import viewsets
from .serializers import UserSerializers, GroupSerializers

ID = 0
email=''


class Login(View):

    @staticmethod
    def redirect_user(user_type):
        global ID
        global email
        url = ''
        user_type = str(user_type)
        if user_type == 'Student':
            student=Student.objects.filter(email=email)
            for i in student:
                ID=i.id
            url = '/student'
        if user_type == 'Teacher':
            teacher=Teacher.objects.filter(email=email)
            for i in teacher:
                ID=i.id
            url = '/teacher'
        if not user_type in ['Teacher', 'Student']:
            url = '/config'
        return url

    @staticmethod
    def get_group_user(id):
        global ID
        global email
        try:
            user_login = User.objects.get(id=id)
            user=User.objects.filter(id=id)
            for i in user:
                email=i.username
            groups = []
            for group in user_login.groups.all():
                groups.append(group)
            return groups[0]
        except:
            pass

    def get(self, request):
        contex = {}
        if not request.user.is_authenticated:
            return render(request, 'school/index.html', contex)
        else:
            user_type = self.get_group_user(request.user.id)
            url = self.redirect_user(user_type)
            return redirect(url)

    def post(self, request):
        url = ''
        contex = {}
        sesion = False
        name = request.POST['username']
        password = request.POST['password']
        credentials = authenticate(username=name, password=password)
        if credentials is not None and credentials.is_active:
            auth_login(request, credentials)
            sesion = True
        else:
            sesion = False
        if sesion:
            print('Loggin')
            user_type = self.get_group_user(request.user.id)
            url = self.redirect_user(user_type)
        else:
            contex = {'contex': 'tu usuario y contraseña es incorrecta'}
            print('loggin failed')

            return render(request, 'school/index.html', contex)
        return redirect(url)


class StudentsViews(LoginRequiredMixin, View):
    @staticmethod
    def verify_student(user):
        ide = user.id
        print(ide)
        log = Login()
        group = log.get_group_user(ide)
        is_student = str(group) == 'Student'
        return is_student


    @staticmethod
    @user_passes_test(verify_student.__func__, login_url='/')
    def student_view(request):
        global ID
        register = Registration.objects.filter(student_id_id=ID)
        notes = Score.objects.all()
        sco1 = []
        sco2 = []
        sco3 = []
        for i in notes:
            if i.registration_id.student_id_id == ID:
                sco1.append(i.score1)
                sco2.append(i.score2)
                sco3.append(i.score3)
        list = []
        for i in register:
            list.append(i.subject_id.name)
        sub = list
        subt = []
        for i in range(0, len(sub)):
            subt.append([sub[i], sco1[i], sco2[i], sco3[i]])
        username = request.user.first_name + " " + request.user.last_name
        subjects = {'Subjects': subt, 'username': username, 'role': "Student"}
        return render(request, 'school/student.html', subjects)


class TeachersViews(LoginRequiredMixin, View):

    @staticmethod
    def verify_teacher(user):
        ide = user.id
        log = Login()
        group = log.get_group_user(ide)
        is_teacher = str(group) == 'Teacher'
        return is_teacher

    @staticmethod
    @user_passes_test(verify_teacher.__func__, login_url='/')
    def teacher_view(request):
        global ID
        register = Registration.objects.filter(subject_id__id_teacher=ID)
        list = []
        count = 0
        for i in register:
            if count != i.subject_id:
                count = i.subject_id
                if i.course_id.grade:
                    list.append([i.subject_id.id,i.subject_id.name])
        context = {'subject': list}

        return render(request, 'school/teacher.html', context)

    @staticmethod
    @user_passes_test(verify_teacher.__func__, login_url='/')
    def teacher_subject_student_view(request):
        context = {}
        return render(request, 'school/teacherSubjectStudent.html', context)



    @staticmethod
    @user_passes_test(verify_teacher.__func__, login_url='/')
    def teacher_subjects_view(request,pk):
        subject = Subject.objects.get(id = pk)
        score = Score.objects.filter(registration_id__subject_id=subject)

        list = []
        for i in score:
            list.append([i.registration_id.student_id, i.score1, i.score2, i.score3])
        context = {'notes': list}
        return render(request, 'school/teacherSubjects.html', context)

    @staticmethod
    @user_passes_test(verify_teacher.__func__, login_url='/')
    def teacher_subject_view(request):
        global ID
        score = Score.objects.filter(registration_id__subject_id=ID)
        list = []
        count = 0
        for i in score:
            list.append([i.registration_id.student_id, i.score1, i.score2, i.score3])
        context = {'notes': list}
        return render(request, 'school/teacherSubject.html', context)


class AdminViews(LoginRequiredMixin, View):

    @staticmethod
    def verify_admin(user):
        ide = user.id
        log = Login()
        group = log.get_group_user(ide)
        is_admin = group == None
        return is_admin

    @staticmethod
    @user_passes_test(verify_admin.__func__, login_url='/')
    def configView(request):
        username = request.user.first_name + " " + request.user.last_name
        teachers = Teacher.objects.all()
        teachers = len(teachers)
        students = Student.objects.all()
        students = len(students)
        subjects = Subject.objects.all()
        subjects = len(subjects)
        records = Registration.objects.all()
        records = len(records)
        courses = Course.objects.all()
        courses = len(courses)
        context = {'teachers': teachers, 'students': students, 'subjects': subjects, 'records': records,
                   'courses': courses, 'username': username, 'role': "Admin"}

        return render(request, 'school/config.html', context)

    @staticmethod
    @user_passes_test(verify_admin.__func__, login_url='/')
    def configTeachersView(request):
        teachers = Teacher.objects.all()
        context = {'teachers': teachers}
        return render(request, 'school/configTeachers.html', context)

    @staticmethod
    @user_passes_test(verify_admin.__func__, login_url='/')
    def configTeacherEditView(request):
        context = {}
        return render(request, 'school/configTeachersEdit.html', context)

    @staticmethod
    @user_passes_test(verify_admin.__func__, login_url='/')
    def configStudentsView(request):
        student = Student.objects.all()
        context = {'students': student}
        return render(request, 'school/configStudents.html', context)

    @staticmethod
    @user_passes_test(verify_admin.__func__, login_url='/')
    def configSubjectsView(request):
        subjects = Subject.objects.all()
        context = {'subjects': subjects}
        return render(request, 'school/configSubjects.html', context)

    @staticmethod
    @user_passes_test(verify_admin.__func__, login_url='/')
    def configSubjectAddView(request):
        if request.method == 'POST':
            form = SubjectForm(request.POST)
            if form.is_valid():
                form.save()
            return redirect('school:configSubjects')
        else:
            form = SubjectForm
        return render(request, 'school/configSubjectsAdd.html', {'form': form})

    @staticmethod
    @user_passes_test(verify_admin.__func__, login_url='/')
    def configSubjectEditView(request):
        context = {}
        return render(request, 'school/configSubjectsEdit.html', context)

    @staticmethod
    @user_passes_test(verify_admin.__func__, login_url='/')
    def configCursosView(request):
        courses = Course.objects.all()
        context = {'courses': courses}
        return render(request, 'school/configCursos.html', context)

    @staticmethod
    @user_passes_test(verify_admin.__func__, login_url='/')
    def configCursosAddView(request):
        if request.method == 'POST':
            form = CourseForm(request.POST)
            if form.is_valid():
                form.save()
            return redirect('school:configCursos')
        else:
           form = CourseForm
        return render(request,'school/configCursosEdit.html',{'form':form})

    @staticmethod
    @user_passes_test(verify_admin.__func__, login_url='/')
    def configCursosEditView(request):
        context = {}
        return render(request, 'school/configCursosEdit.html', context)

    @staticmethod
    @user_passes_test(verify_admin.__func__, login_url='/')
    def configEnrollmentsView(request):
        registers = Registration.objects.all()
        context = {'registers': registers}
        return render(request, 'school/configEnrollments.html', context)

    @staticmethod
    @user_passes_test(verify_admin.__func__, login_url='/')
    def configEnrollmentsAddView(request):
        if request.method == 'POST':
            form = RegistrationForm(request.POST)
            if form.is_valid():
                form.save()
            return redirect('school:configEnrollments')

        else:

            form = RegistrationForm

        return render(request, 'school/configEnrollmentsAdd.html', {'form': form})

    @staticmethod
    @user_passes_test(verify_admin.__func__, login_url='/')
    def configEnrollmentsEditView(request):
        context = {}
        return render(request, 'school/configEnrollmentsEdit.html', context)


class AddStudentCreateView(LoginRequiredMixin, CreateView):
    model = Student
    template_name = 'school/configStudentsAdd.html'
    form_class = StudentForm
    second_form_class = UserForm
    success_url = reverse_lazy('school:configStudents')

    def get_context_data(self, **kwargs):
        context = super(AddStudentCreateView, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class(self.request.GET)
        if 'form2' not in context:
            context['form2'] = self.second_form_class(self.request.GET)

        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        form2 = self.second_form_class(request.POST)
        if form.is_valid() and form2.is_valid():
            form.save()
            user = form2.save(commit=False)
            user.username = request.POST.get('email')
            user.email = request.POST.get('email')
            user.first_name = request.POST.get('name')
            user.last_name = request.POST.get('last_name')
            group = Group.objects.get(name='Student')
            group.user_set.add(form2.save())
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form, form2=form2))


class ConfigStudentEditView(LoginRequiredMixin, UpdateView):
    model = Student
    second_model = User
    template_name = 'school/configStudentsAdd.html'
    form_class = StudentForm
    second_form_class = UserForm
    success_url = reverse_lazy('school:configStudents')

    def get_context_data(self, **kwargs):
        context = super(ConfigStudentEditView, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        student = self.model.objects.get(id=pk)
        user = self.second_model.objects.get(username=student.email)
        if 'form' not in context:
            context['form'] = self.form_class()
        if 'form2' not in context:
            context['form2'] = self.second_form_class(instance=user)
        context['id'] = pk
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        id = kwargs['pk']
        student = self.model.objects.get(id=id)
        user = self.second_model.objects.get(username=student.email)
        form = self.form_class(request.POST)
        form2 = self.second_form_class(request.POST)
        if form.is_valid() and form2.is_valid():
            form.save()
            user = form2.save(commit=False)
            user.username = request.POST.get('email')
            user.email = request.POST.get('email')
            user.first_name = request.POST.get('name')
            user.last_name = request.POST.get('last_name')
            group = Group.objects.get(name='student')
            group.user_set.add(form2.save())
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form, form2=form2))


class AddTeacherCreateView(LoginRequiredMixin, CreateView):
    model = Teacher
    template_name = 'school/configTeachersAdd.html'
    form_class = TeacherForm
    second_form_class = UserForm
    success_url = reverse_lazy('school:configTeachers')

    def get_context_data(self, **kwargs):
        context = super(AddTeacherCreateView, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class(self.request.GET)
        if 'form2' not in context:
            context['form2'] = self.second_form_class(self.request.GET)

        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        form2 = self.second_form_class(request.POST)
        if form.is_valid() and form2.is_valid():
            form.save()
            user = form2.save(commit=False)
            user.username = request.POST.get('email')
            user.email = request.POST.get('email')
            user.first_name = request.POST.get('name')
            user.last_name = request.POST.get('last_name')
            group = Group.objects.get(name='Teacher')
            group.user_set.add(form2.save())
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form, form2=form2))


def index(request):
    context = {}
    return render(request, 'school/index.html', context)

def index(request):
    r2 = {}
    #template_name = 'school/index.html'
    return render(request, 'registration/logged_out.html', r2)

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializers

class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializers
