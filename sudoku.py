sudukuInicial = [[8, 0, 0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 3, 6, 0, 0, 0, 0, 0],
                 [0, 7, 0, 0, 9, 0, 2, 0, 0],
                 [0, 5, 0, 0, 0, 7, 0, 0, 0],
                 [0, 0, 0, 0, 4, 5, 7, 0, 0],
                 [0, 0, 0, 1, 0, 0, 0, 3, 0],
                 [0, 0, 1, 0, 0, 0, 0, 6, 8],
                 [0, 0, 8, 5, 0, 0, 0, 1, 0],
                 [0, 9, 0, 0, 0, 0, 4, 0, 0]]
sudoku = sudukuInicial

def fila_columna(tabla, fila, columna, numero):
    columnaT = []
    for i in range(len(tabla)):
        columnaT.append(tabla[i][columna])
    if tabla[fila].count(numero) > 0 or columnaT.count(numero) > 0 or buscaGrupo(tabla, fila, columna, numero) > 0 or tabla[fila][columna] > 0:
        return 1
    else:
        return 0

def buscaRango(numero):
    if numero < 3:
        rango = 0;
    elif numero > 2 and numero < 6:
        rango = 3;
    elif numero > 5 and numero < 9:
        rango = 6;
    return rango;


def buscaGrupo(tabla, fila, columna, numero):
    filaH = buscaRango(fila)
    columnaH = buscaRango(columna)
    grupo = []
    for i in range(filaH, filaH + 3):
        for j in range(columnaH, columnaH + 3):
            grupo.append(tabla[i][j])
    if grupo.count(numero) > 0:
        return 1
    else:
        return 0

def llenar(tabla, fila, columna, valor):
    if fila_columna(tabla, fila, columna, valor) == 0:
        tabla[fila][columna] = valor
        return 0
    else:
        return 1

def resolverSudoku(tabla, fila, columna):
    if columna >= 9:
        columna = 0;
        fila = fila + 1
    if fila == 9:
        return True
    if sudoku[fila][columna] is not 0:
        if resolverSudoku(tabla, fila, columna + 1):
            return True
    for i in range(1, 10):
        if (llenar(tabla, fila, columna, i) == 0):
            if resolverSudoku(tabla, fila, columna + 1):
                return True
            tabla[fila][columna] = 0
    return False

resolverSudoku(sudukuInicial, 0, 0)
for i in sudoku:
    print(i)