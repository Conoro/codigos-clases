import socketserver
import http.server
from http.server import HTTPServer,BaseHTTPRequestHandler
import os
port=8000
Handler= http.server.SimpleHTTPRequestHandler
rutas = {
           '/': 'index.html',
           '/about': 'about.html',
           '/contact': 'contact.html',
           '/home':'home.html',
           '/estilo.css':'estilo.css'
       }
class Staticserver(BaseHTTPRequestHandler):
   def do_GET(self):
        root = os.path.join(os.path.dirname(os.path.abspath(__file__))),'servidor estatico'
        if self.path=='/estilo.css':
            content = self.handle_http(200, 'text/css')
            self.wfile.write(content)
        if self.path=='/' or self.path=='/about' or self.path=='/contact' or self.path=='/home':
            content = self.handle_http(200, 'text/html')
            self.wfile.write(content)
        else:
            content = self.handle_http(404, 'text/html')
            self.wfile.write(content)
   def handle_http(self,status,conten_type):
       
       self.send_response(status)
       self.end_headers()
       if status==200:
           routes_content= rutas[self.path]
           f= open(routes_content,'r')
       elif status==404:
           f=open('error.html','r')
       return bytes(f.read(), 'UTF-8')   
def run(server_class=HTTPServer,handler_clas=Staticserver,port=8000):
    server_adress=('',port)
    httpd=server_class(server_adress,handler_clas)
    print("iniciando en el puerto http {}".format(port))
    httpd.serve_forever()

run()
 